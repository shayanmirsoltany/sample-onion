﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DA.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceImp.ServicesImpl;

namespace sample_mirsoltani.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        [HttpGet]
        //[Route("api/Student/GetStudents")]
        public ActionResult<IEnumerable<Student>> GetTest()
        {
            var lst = _studentService.GetAll().ToList();
            return lst;
        }

        [HttpPost]
        //[Route("api/Student/AddStudent")]
        public ActionResult<object> AddStudent()
        {
            var student = new Student() { Age = 27, FirstName = "shayan", LastName = "Mirsoltani", GPA = 19, Status = 0 };// _studentService.Add();
            _studentService.Add(student);
            var result = _studentService.Commit();
            return student;
        }
    }
}